<?php

/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2015 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */
class ET_PaymentRobokassa_Block_Error extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
        $storeId = Mage::app()->getStore()->getId();
        $html = '<html><body>';
        $html .= $this->__('An error has occurred during the checkout process. Please, contact the store owner.');
        $html .= '<br><br><a href="' .
            Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . '">' .
            $this->__('Back to the main page') . '</a>';
        $html .= '</body></html>';
        return $html;
    }

}
