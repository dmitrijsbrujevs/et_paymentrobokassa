<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

// Нужен из-за того, что платёжный сервер в любом случае
// возвращает на один и тот же адрес, независимо
// от успеха операции и языка.

class ET_PaymentRobokassa_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    protected $_routerController;
    protected $_routerName = 'etrobokassa';
    protected $_moduleName = 'etpaymentrobokassa';


    public function initControllerRouters($observer)
    {
        $front = $observer->getEvent()->getFront();
        $this->setRouterController();
        $front->addRouter($this->_routerName, $this->_routerController);
    }

    public function setRouterController()
    {
        $this->_routerController = new ET_PaymentRobokassa_Controller_Router();
    }

    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        $identifier = trim($request->getPathInfo(), '/');
        $allpath = explode("/", $identifier);

        //ne nash modulj
        if ($this->_isOurModule($allpath[0])) {
            return false;
        }

        /* to avoid /gate/ in return urls */
        $allpath[2] = $allpath[1];
        $allpath[1] = 'gate';

        if ($orderId = $request->getParam("InvId")) {
            Mage::app()->getStore()->load(Mage::getModel("sales/order")->load($orderId)->getStoreId());
        }
        // var_dump($allpath);
        //exit();
        $request->setModuleName($this->_moduleName)
            ->setControllerName(isset($allpath[1]) ? $allpath[1] : 'gate')
            ->setActionName(isset($allpath[2]) ? $allpath[2] : 'success');
        $request->setAlias(
            Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
            trim($request->getPathInfo(), '/')
        );

        return true;
    }

    /**
     * @param $allpath
     * @return bool
     */
    protected function _isOurModule($urlKey)
    {
        return ($urlKey != $this->_routerName);
    }
}