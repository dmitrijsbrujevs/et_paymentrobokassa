<?php

/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */
class ET_PaymentRobokassa_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function refillCart($order)
    {
        //probujem zanovo nabitj korzinu
        $cartRefilled = true;

        $cart = Mage::getSingleton('checkout/cart');
        $items = $order->getItemsCollection();
        foreach ($items as $item) {
            try {
                $cart->addOrderItem($item);
            } catch (Mage_Core_Exception $e) {
                $cartRefilled = false;
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                } else {
                    Mage::getSingleton('checkout/session')->addError($e->getMessage());
                }
                $this->_redirect('customer/account/history');
            } catch (Exception $e) {
                $cartRefilled = false;
                Mage::getSingleton('checkout/session')->addException(
                    $e,
                    Mage::helper('checkout')->__('Cannot add the item to shopping cart.')
                );
            }
        }
        $cart->save();

        return $cartRefilled;
    }

    public function log($message)
    {
        if ($this->isLogEnabled()) {
            $file = $this->getLogFileName();
            if (is_array($message)) {
                Mage::dispatchEvent('robokassa_log_file_write_before', $message);
                $forLog = array();
                foreach ($message as $answerKey => $answerValue) {
                    $forLog[] = $answerKey . ": " . (is_array($answerValue) ? print_r($answerValue, true) : $answerValue);
                }
                $forLog[] = '***************************';
                $message = implode("\r\n", $forLog);
            }
            Mage::log($message, Zend_Log::DEBUG, $file, true);
        }
        return true;
    }

    public function arrayToRawData($array)
    {
        foreach ($array as $key => $value) {
            $newArray[] = $key . ": " . $value;
        }
        $raw = implode("\r\n", $newArray);
        return $raw;
    }

    public function isLogEnabled()
    {
        return Mage::getStoreConfig('payment/etrobokassa/enable_log');
    }

    public function getLogFileName()
    {
        return 'et_robokassa.log';
    }

}
