<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

class ET_PaymentRobokassa_Model_Method_Etrobokassa extends Mage_Payment_Model_Method_Abstract
{

    protected $_code = 'etrobokassa';
    protected $_formBlockType = 'etpaymentrobokassa/single_form';
    protected $_infoBlockType = 'etpaymentrobokassa/single_info';

    /**
     * Payment Method features
     * @var bool
     */
    protected $_isGateway = true;
    protected $_canOrder = false;
    protected $_canAuthorize = false;
    protected $_canCapture = true;
    protected $_canCapturePartial = false;
    protected $_canRefund = false;
    protected $_canRefundInvoicePartial = false;
    protected $_canVoid = false;
    protected $_canUseRobonal = false;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;
    protected $_isInitializeNeeded = false;
    protected $_canFetchTransactionInfo = false;
    protected $_canReviewPayment = false;
    protected $_canCreateBillingAgreement = false;
    protected $_canManageRecurringProfiles = false;
    protected $_canEdit = false;

    protected $_canUseInternal = false; // Payment method will not work in admin panel order

    /**
     * Payment Method instance configuration
     * @var bool
     */
    protected $_isActive = 0;
    protected $_title;
    protected $_description;
    protected $_testMode;

    protected $_isLogenabled = 0;


    protected $_sMerchantLogin;
    protected $_sInvDesc;
    protected $_paymentText;
    protected $_sIncCurrLabel;
    protected $_sCulture;
    protected $_transferCurrency;


    protected $_sMerchantPassOne;
    protected $_sMerchantPassTwo;

    protected $_cartRefill;

    protected $_gateUrl = "https://merchant.roboxchange.com/Index.aspx";
    //protected $_testUrl="shell/rktest.php";
    protected $_testUrl = "http://test.robokassa.ru/Index.aspx";


    protected $_configRead = false;


    public function __construct()
    {
        parent::__construct();
        $this->readConfig();
    }


    protected function readConfig()
    {
        if ($this->_configRead) {
            return;
        }
        $this->_isActive = $this->getConfigData('active');
        $this->_title = $this->getConfigData('title');
        $this->_testMode = $this->getConfigDataRobo('test_mode');

        $this->_paymentText = $this->getConfigData('payment_text');
        $this->_description = Mage::helper('cms')->getBlockTemplateProcessor()->filter($this->_paymentText);


        $this->_sMerchantLogin = $this->getConfigDataRobo('sMerchantLogin');

        $this->_sInvDesc = $this->getConfigDataRobo('sInvDesc');

        $this->_sIncCurrLabel = $this->getConfigData('sIncCurrLabel');
        $this->_transferCurrency = Mage::app()->getBaseCurrencyCode();

        $this->_sCulture = $this->getConfigDataRobo('sCulture');

        $this->_sMerchantPassOne = Mage::helper('core')->decrypt($this->getConfigDataRobo('sMerchantPass1'));
        $this->_sMerchantPassTwo = Mage::helper('core')->decrypt($this->getConfigDataRobo('sMerchantPass2'));

        $this->_cartRefill = $this->getConfigDataRobo('cart_refill');

        $this->_configRead = true;

        return;
    }

    /**
     * To read common settings, like login and passwords.
     * all specific methot setting must be read with default function
     *
     */
    public function getConfigDataRobo($field, $storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStore();
        }
        $path = 'payment/etrobokassa/' . $field;
        return Mage::getStoreConfig($path, $storeId);
    }


    public function getDescription()
    {
        $this->readConfig();
        return $this->_description;
    }


    public function getOrderPlaceRedirectUrl()
    {
        return $this->getRedirectUrl();
    }

    public function getRedirectUrl()
    {
        return Mage::getUrl('etrobokassa/redirect');
    }

    /**
     * Check whether payment method can be used
     * @param Mage_Sales_Model_Quote
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        return parent::isAvailable($quote);
    }

    public function canUseForCurrency($currencyCode)
    {
        $baseCurrency = Mage::app()->getWebsite()->getBaseCurrency();
        $rate = $baseCurrency->getRate($this->_transferCurrency);
        $displayCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $reverseRate = $baseCurrency->getRate($displayCurrencyCode);
        if (!$rate || !$reverseRate) {
            Mage::helper("etpaymentrobokassa")->log('There is no rate for [' . $displayCurrencyCode . "/"
                . $this->_transferCurrency
                . '] to convert order amount. Payment method Robokassa not displayed.');
            return false;
        }
        return true;
    }

    public function canUseForCountry($country)
    {
        if (Mage::getStoreConfig('payment/etrobokassa/allowspecific') == 1) {
            $availableCountries = explode(',', Mage::getStoreConfig('payment/etrobokassa/specificcountry'));
            if (!in_array($country, $availableCountries)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @return array
     */
    public function preparePaymentData($order)
    {
        $this->readConfig();

        if (empty($this->_sMerchantLogin) || empty($this->_sMerchantPassOne) || empty($this->_sMerchantPassTwo)) {
            Mage::helper("etpaymentrobokassa")
                ->log('Please enter login information about your Robokassa merchant in admin panel!');
        }

        $hash = $this->generateHash($order, $writeLog = true);
        $outSum = $this->getOutSum($order);
        $postData = array(
            "MrchLogin" => $this->_sMerchantLogin,
            "OutSum" => round($outSum, 2),
            //"InvId" => $order->getId(),
            "InvId" => $order->getIncrementId(),
            "Desc" => $this->_sInvDesc,
            "SignatureValue" => $hash,
            "IncCurrLabel" => $this->_sIncCurrLabel,
            "Email" => $order->getCustomerEmail(),
            "Culture" => $this->_sCulture
        );
        $result = array(
            'postData' => new Varien_Object($postData),
            'order' => $order,
        );
        Mage::dispatchEvent('robokassa_prepare_payment_data', $result);
        $postData = $result['postData']->getData();
        return $postData;
    }

    public function checkAnswerData($answer)
    {
        try {
            $errors = array();
            $this->readConfig();
            //$order = Mage::getModel("sales/order")->load($this->getOrderId($answer));
            $order = Mage::getModel("sales/order")->loadByIncrementId($this->getOrderId($answer));
            $hashArray = array(
                $answer["OutSum"],
                $answer["InvId"],
                $this->_sMerchantPassTwo
            );

            $hashCurrent = strtoupper(md5(implode(":", $hashArray)));
            $correctHash = (strcmp($hashCurrent, strtoupper($answer['SignatureValue'])) == 0);

            if (!$correctHash) {
                $errors[] = "Incorrect HASH (need:" . $hashCurrent . ", got:"
                    . strtoupper($answer['SignatureValue']) . ") - fraud data or wrong secret Key";
                $errors[] = "Maybe success payment";
            }

            /**
             * @var $order Mage_Sales_Model_Order
             */
            if ($this->_transferCurrency != $order->getOrderCurrencyCode()) {
                $outSum = round(
                    $order->getBaseCurrency()->convert($order->getBaseGrandTotal(), $this->_transferCurrency),
                    2
                );
            } else {
                $outSum = round($order->getGrandTotal(), 2);
            }

            if ($outSum != $answer["OutSum"]) {
                $errors[] = "Incorrect Amount: " . $answer["OutSum"] . " (need: " . $outSum . ")";
            }

            if (count($errors) > 0) {
                return $errors;
            }

            return (bool)$correctHash;
        } catch (Exception $e) {
            return array("Internal error:" . $e->getMessage());
        }
    }


    public function getGateUrl()
    {
        $this->readConfig();
        if ($this->_testMode) {
            return $this->_testUrl;
        }
        return $this->_gateUrl;
    }


    public function getOrderId($answer)
    {
        return isset($answer["InvId"]) ? $answer["InvId"] : "";
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @param bool $writeLog
     *
     * @return string
     */
    public function generateHash($order, $writeLog = false)
    {
        $outSum = $this->getOutSum($order, $writeLog);
        $hashData = array(
            "MrchLogin" => $this->_sMerchantLogin,
            "OutSum" => round($outSum, 2),
            //"InvId" => $order->getId(),
            "InvId" => $order->getIncrementId(),
            "pass" => $this->_sMerchantPassOne,
        );

        $hash = strtoupper(md5(implode(":", $hashData)));
        return $hash;
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @param bool $writeLog
     *
     * @return float
     */
    public function getOutSum($order, $writeLog = false)
    {
        if ($this->_transferCurrency != $order->getOrderCurrencyCode()) {
            $outSum = $order->getBaseCurrency()->convert($order->getBaseGrandTotal(), $this->_transferCurrency);
            if ($writeLog) {
                Mage::helper("etpaymentrobokassa")->log('Currency converted from [' .
                    Mage::app()->getStore()->getCurrentCurrencyCode() . '] to [' .
                    $this->_transferCurrency . '] ' . $order->getBaseGrandTotal() . ' ' . $outSum . '');
            }
        } else {
            $outSum = $order->getGrandTotal();
        }

        return $outSum;
    }

}