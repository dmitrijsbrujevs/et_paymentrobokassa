<?php

/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */
class ET_PaymentRobokassa_GateController extends Mage_Core_Controller_Front_Action
{
    const MODULENAME = "etpaymentrobokassa";
    const PAYMENTNAME = "etrobokassa";

    /**
     * Setting right header of response if session died
     *
     */
    protected function _expireAjax()
    {
        if (!Mage::getSingleton('checkout/session')->getQuote()->hasItems()) {
            $this->getResponse()->setHeader('HTTP/1.1', '403 Session Expired');
            exit;
        }
    }

    public function redirectAction()
    {
        /** @var  $helper ET_PaymentRobokassa_Helper_Data */
        $helper = Mage::helper("etpaymentrobokassa");
        $session = Mage::getSingleton('checkout/session');
        $state = Mage_Sales_Model_Order::STATE_NEW;

        //todo брать $status из настроек подмодулей
        $status = Mage::getStoreConfig('payment/etrobokassa/order_status');
        if (strlen($status) == 0) {
            //$status = 'pending';
            $status = Mage::getModel('sales/order')->getConfig()->getStateDefaultStatus($state);
        }

        /* @var $order Mage_Sales_Model_Order */
        $lastOrderId = $session->getLastOrderId();
        if (empty($lastOrderId)) {
            $helper->log("Error. Redirect to robokassa failed. No data in session about order.");
            $this->getResponse()->setHeader('Content-type', 'text/html; charset=UTF8');
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('etpaymentrobokassa/error')->toHtml());
            return;
        }
        $order = Mage::getModel('sales/order')->load($lastOrderId);
        $order->setState($state,
            $status,
            $this->__('Customer redirected to payment Gateway Robokassa'),
            false);
        $order->save();

        $payment = $order->getPayment()->getMethodInstance();
        if (!$payment) {
            $payment = Mage::getSingleton("etpaymentrobokassa/method_etrobokassa");
        }
        $dataForSending = $payment->preparePaymentData($order);

        $dataForSending = array_merge(array('Data transfer' => 'To Robokassa'), $dataForSending);
        $helper->log($dataForSending);
        $this->getResponse()->setHeader('Content-type', 'text/html; charset=UTF8');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('etpaymentrobokassa/redirect')->setGateUrl(
                $payment->getGateUrl())->setPostData($dataForSending)->toHtml()
        );
    }

    public function statusAction()
    {
        /** @var  $helper ET_PaymentRobokassa_Helper_Data */
        $helper = Mage::helper("etpaymentrobokassa");
        $state = Mage_Sales_Model_Order::STATE_PROCESSING;
        $errorStatus = 'error_robokassa';
        $answer = $this->getRequest()->getParams();
        $payment = Mage::getSingleton(self::MODULENAME . "/method_" . self::PAYMENTNAME);
        if ($payment->getOrderId($answer)) {
            /* @var $order Mage_Sales_Model_Order */
            //$order = Mage::getModel('sales/order')->load($payment->getOrderId($answer));
            $order = Mage::getModel('sales/order')->loadByIncrementId($payment->getOrderId($answer));
            //true un success, false on fail or array of text on error

            $orderPaymentMethod = $order->getPayment()->getMethodInstance();

            $checkedAnswer = $orderPaymentMethod->checkAnswerData($answer);

            if (is_array($checkedAnswer)) {
                $answer['Errors'] = $helper->arrayToRawData($checkedAnswer);
            }
            $result = array(
                'answer' => new Varien_Object($answer),
                'order' => $order,
            );
            Mage::dispatchEvent('robokassa_success_answer', $result);
            $answer = $result['answer']->getData();
            $answer = array_merge(array('Data transfer' => 'From Robokassa'), $answer);

            $helper->log($answer);

            if ($checkedAnswer === true) {
                Mage::dispatchEvent('robokassa_success_answer_without_error', $result);
                if ($order->getState() == Mage_Sales_Model_Order::STATE_NEW) {
                    if ($order->canInvoice()) {
                        //for partial invoice
                        if (!($qtys = $orderPaymentMethod->getInvoiceQtys($order))) {
                            $qtys = array();
                        }
                        $invoice = $order->prepareInvoice($qtys);
                        $invoice->register();
                        $invoice->capture();
                        $order->addRelatedObject($invoice);
                    }
                    if (!($paidStatus = $orderPaymentMethod->getPaidStatus())) {
                        $paidStatus = 'paid_robokassa';
                    }
                    $result = $this->_sendEmailAfterPaymentSuccess($order);

                    $order->setState($state,
                        $paidStatus,
                        $this->__($helper->__('The amount has been authorized and captured by Robokassa.')),
                        $result);
                    $order->save();
                }
                print 'OK' . $orderPaymentMethod->getOrderId($answer);
            } else {
                if ($order->getId()) {
                    $order->addStatusToHistory($errorStatus, implode(" / ", $checkedAnswer), false);
                    $order->save();
                }
                print "Error";
            }
        } else {
            $errorAnswer = 'Incorrect answer. No order number.';
            $helper->log($errorAnswer);
            if (!$answer) {
                $answer = 'No answer data';
            }
            $helper->log($answer);
            print($errorAnswer);
            //$this->_redirect('/');
        }
    }

    public function failureAction()
    {
        /** @var  $helper ET_PaymentRobokassa_Helper_Data */
        $helper = Mage::helper("etpaymentrobokassa");

        $payment = Mage::getSingleton(self::MODULENAME . "/method_" . self::PAYMENTNAME);
        $answer = $this->getRequest()->getParams();

        if ($payment->getOrderId($answer)) {
            /** @var $order Mage_Sales_Model_Order */
            //$order = Mage::getModel('sales/order')->load($payment->getOrderId($answer));
            $order = Mage::getModel('sales/order')->loadByIncrementId($payment->getOrderId($answer));

            $result = array(
                'answer' => new Varien_Object($answer),
                'order' => $order,
            );

            Mage::dispatchEvent('robokassa_failure_answer', $result);
            $answer = $result['answer']->getData();
            $answer = array_merge(array('Data transfer' => 'From Robokassa'), $answer);
            $helper->log($answer);
            if (Mage::getStoreConfig('payment/' . self::PAYMENTNAME . '/cart_refill')) {
                $helper->refillCart($order);
            }

            $order->addStatusToHistory(
                $order->getStatus(),
                $helper->__('Payment failed'),
                false
            );
            $order->save();
            $order->cancel()->save();

            $this->_redirect('checkout/onepage/failure');
        } else {
            $errorAnswer = 'Incorrect answer. No order number.';
            $helper->log($errorAnswer);
            if (!$answer) {
                $answer = 'No answer data';
            }
            $helper->log($answer);
            print($errorAnswer);
            //$this->_redirect('/');
        }
    }

    public function successAction()
    {
        /** @var $session Mage_Checkout_Model_Session */
        $session = Mage::getSingleton('checkout/session');
        if (!$session->getLastOrderId() || !$session->getLastQuoteId() || !$session->getLastSuccessQuoteId()) {
            $answer = $this->getRequest()->getParams();
            Mage::dispatchEvent('robokassa_no_session_data_for_success', $answer);
        }
        $this->_redirect("checkout/onepage/success");
    }

    /**
     * Send new order email
     * @param $order Mage_Sales_Model_Order
     * @return bool
     */
    protected function _sendEmailAfterPaymentSuccess($order)
    {
        if ($order->sendNewOrderEmail()) {
            $result = true;
            $order->setEmailSent(true);
        } else {
            $result = false;
        }
        return $result;
    }
}