== Extension links
Permanent link: http://shop.etwebsolutions.com/eng/et-payment-robokassa.html
Support link: http://support.etwebsolutions.com/projects/et-payment-robokassa/roadmap

== Short Description
Payment extension Robokassa.
Allows to use payment service Robokassa (robokassa.ru) on your Magento site.

Implemented only basic features.
Additional features are available in the extension ET_PaymentRobokassaAdvanced.

== Version Compatibility
Magento CE:
1.3.x
1.4.x
1.5.x (tested in 1.5.1.0)
1.6.x (tested in 1.6.2.0)
1.7.x (tested in 1.7.0.2)
1.8.x (tested in 1.8.0.0)
1.9.x (tested in 1.9.0.1)

== Installation
* Disable compilation if it is enabled (System -> Tools -> Compilation)
* Disable cache if it is enabled (System -> Cache Management)
* Download the extension or install the extension from Magento Connect
* Copy all files from the "install" folder to the Magento root folder - where your index.php is
* Log out from the admin panel
* Log in to the admin panel with your login and password
* Set extension's parameters (System -> Configuration -> ET EXTENSIONS -> Payment Robokassa)
* Run compilation process and enable cache if needed
